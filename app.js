require('newrelic');
require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('./config/passport');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const User = require('./models/user');
const Token = require('./models/token');
const jwt = require('jsonwebtoken');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var tokenRouter = require('./routes/token');
var bikesRouter = require('./routes/bikes');
var authAPIRouter = require('./routes/api/auth');
var bikesAPIRouter = require('./routes/api/bikes');
var usersAPIRouter = require('./routes/api/users');
var reservationsAPIRouter = require('./routes/api/reservations');

let store;
if (process.env.NODE_ENV === 'development') {
  store = new session.MemoryStore();
} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });
  store.on('error', function (error) {
    assert.ifError(error);
    assert.ok(false);
  });
}

var app = express();

app.set('secretKey', 'jwt_pwd_987654321');

app.use(
  session({
    cookie: { maxAge: 240 * 60 * 60 * 1000 },
    store: store,
    saveUninitialized: true,
    resave: true,
    secret: 'red_bici!"#$%&'
  })
);

// MongoDB connection
var mongoose = require('mongoose');
const { assert } = require('console');
var mongoDB = process.env.MONGO_URI;
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MondoDB connection error'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/login', function (req, res) {
  res.render('session/login');
});

app.post('/login', function (req, res, next) {
  passport.authenticate('local', function (err, user, info) {
    if (err) return next(err);
    if (!user) return res.render('session/login', { info });
    req.logIn(user, function (err) {
      if (err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);
});

app.get('/logout', function (req, res) {
  req.logOut();
  res.redirect('/');
});

app.get('/forgotpassword', function (req, res, next) {
  res.render('session/forgotPassword');
});

app.post('/forgotPassword', function (req, res) {
  User.findOne({ email: req.body.email }, function (err, user) {
    if (!user) return res.render('session/forgotPassword', { info: { message: ' No existe correo' } });
    user.resetPassword(function (err) {
      if (err) return next(err);
      console.log('session/forgotPasswordMessage');
    });

    res.render('session/forgotPasswordMessage');
  });
});

app.get('/resetPassword/:token', function (req, res, next) {
  if (!req.params.token) {
    return res.status(400).send({ type: 'not-verified', msg: 'No se encuentra Token' });
  }

  Token.findOne({ token: req.params.token }, function (err, token) {
    if (!token)
      return res
        .status(400)
        .send({ type: 'not-verified', msg: 'No existe un usuario asociado al Token. Verifique que su token no haya expirado' });

    User.findById(token._userId, function (err, user) {
      if (!user) return res.status(400).send({ msg: 'No existe un usuario asociado al token' });
      res.render('session/resetPassword', { errors: {}, user: user });
    });
  });
});

app.post('/resetPassword', function (req, res) {
  if (req.body.password !== req.body.confirm_password) {
    res.render('session/resetPassword', {
      errors: { confirm_password: { message: 'No coincide con el password ingresado' } },
      user: new User({ email: req.body.email })
    });
  }
  User.findOne({ email: req.body.email }, function (err, user) {
    user.password = req.body.password;
    user.save(function (err) {
      if (err) {
        res.render('session/resetPassword', { errors: err.errors, user: new User({ email: req.body.email }) });
      } else {
        res.redirect('/login');
      }
    });
  });
});

function loggedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    console.log('user not logged');
    res.redirect('/login');
  }
}

function validateUser(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {
    if (err) {
      res.json({ status: 'error', message: err.message, data: null });
    } else {
      req.body.userId = decoded.id;
      console.log('jwt verify: ' + decoded);
      next();
    }
  });
}

app.use('/users', loggedIn, usersRouter);
app.use('/token', tokenRouter);
app.use('/bicicletas', loggedIn, bikesRouter);

// api routes
app.use('/api/auth', authAPIRouter);
app.use('/api/bicicletas', validateUser, bikesAPIRouter);
app.use('/api/users', usersAPIRouter);
app.use('/api/reservas', validateUser, reservationsAPIRouter);
app.use('/privacy_policy', function (req, res, next) {
  res.sendFile('./public/privacy_policy.html');
});
app.use('/google78444e3930584d2f', function (req, res, next) {
  res.sendFile('./public/google78444e3930584d2f.html');
});

app.get(
  '/auth/google',
  passport.authenticate('google', {
    scope: ['https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/userinfo.email']
  })
);

app.get('/auth/google/callback', passport.authenticate('google', { failureRedirect: '/login' }), function (req, res) {
  // Successful authentication, redirect home.
  res.redirect('/');
});

app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
