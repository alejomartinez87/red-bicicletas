var express = require('express');
var router = express.Router();
var reservationController = require('../../controllers/api/reservationControllerAPI');

router.get('/', reservationController.reservation_list);
router.post('/create', reservationController.reservation_create);
router.delete('/delete', reservationController.reservation_delete);

module.exports = router;
