if (map != undefined || map != null) {
  map.remove();
  $('#map').html('');
  $('#preMap').empty();
  $('<div id="map" style="height: 500px;"></div>').appendTo('#preMap');
}

var map = L.map('map').setView([6.2648627, -75.566595], 14);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

// L.marker([6.2648627, -75.566595]).addTo(map);
// L.marker([6.2621391, -75.5784817]).addTo(map);
// L.marker([6.274474, -75.588582]).addTo(map);

$.ajax({
  dataType: 'json',
  url: 'api/bicicletas',
  success: function (result) {
    console.log(result);
    result.bikes.forEach(function (bike) {
      L.marker(bike.ubication, { title: bike.id }).addTo(map);
    });
  }
});
