var mongoose = require('mongoose');
var Bike = require('../../models/bike');

describe('Testing Bike Model', () => {
  // Test before All
  beforeAll((done) => {
    mongoose.connection.close(done);
  });

  // Test before each
  beforeEach(function (done) {
    var mongoDB = 'mongodb://localhost/testdb';
    mongoose.connect(mongoDB, { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true });
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', function () {
      console.log('We are connected to test database');
      done();
    });
  });

  // Test after each
  afterEach(function (done) {
    Bike.deleteMany({}, function (err, success) {
      if (err) {
        console.log(err);
      }
      done();
      mongoose.disconnect();
    });
  });

  // Bike Create Instance
  describe('Bike.createInstance', () => {
    it('crea una instancia de Bicicleta', () => {
      var bike = Bike.createInstance(1, 'verde', 'urbana', [-34.5, -54.1]);

      expect(bike.code).toBe(1);
      expect(bike.color).toBe('verde');
      expect(bike.model).toBe('urbana');
      expect(bike.ubication[0]).toEqual(-34.5);
      expect(bike.ubication[1]).toEqual(-54.1);
    });
  });

  //Bike.allBikes
  describe('Bike.allBikes', () => {
    it('comienza vacía', (done) => {
      Bike.allBikes(function (err, bikes) {
        if (err) console.log(err);
        expect(bikes.length).toEqual(0);
        done();
      });
    });
  });

  //Bike.add

  describe('Bike.add', () => {
    it('agrega solo una bike', (done) => {
      var bike = new Bike({ code: 1, color: 'verde', model: 'urbana' });

      Bike.add(bike, function (err, newBike) {
        if (err) {
          console.log('Error Bike.add', err);
        }
        Bike.allBikes(function (err, bikes) {
          expect(bikes.length).toEqual(1);
          expect(bikes[0].code).toEqual(bike.code);
          done();
        });
      });
    });
  });

  // Bike.findByCode
  describe('Bike.findByCode', () => {
    it('debe devolver la bici con code 1', (done) => {
      Bike.allBikes(function (err, bikes) {
        expect(bikes.length).toBe(0);
      });

      var aBike = new Bike({ code: 1, color: 'verde', modelo: 'urbana' });
      Bike.add(aBike, function (err, newBike) {
        if (err) console.log(err);

        var aBike2 = new Bike({ code: 2, color: 'roja', model: 'urbana' });
        Bike.add(aBike2, function (err, newBike) {
          if (err) console.log(err);
          Bike.findByCode(1, function (error, targetBike) {
            expect(targetBike.code).toBe(aBike.code);
            expect(targetBike.color).toBe(aBike.color);
            expect(targetBike.model).toBe(aBike.model);
            done();
          });
        });
      });
    });
  });
});
