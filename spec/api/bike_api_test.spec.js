var mongoose = require('mongoose');
var Bike = require('../../models/bike');
var request = require('request');
var server = require('../../bin/www');

var base_url = 'http://localhost:3000/api/bicicletas';

describe('Bike API', () => {
  beforeAll((done) => {
    mongoose.connection.close(done);
  });

  beforeEach(function (done) {
    let mongoDB = 'mongodb://localhost/testdb';
    mongoose.connect(mongoDB, { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', function () {
      console.log('We are connected to test database!');
      done();
    });
  });

  afterEach(function (done) {
    Bike.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
      mongoose.disconnect();
    });
  });

  // Test GET
  describe('GET /api/bicicletas', () => {
    it('Status 200', (done) => {
      Bike.find({}, (err, bikes) => {
        expect(bikes.length).toBe(0);
      });

      var aBike = new Bike({ code: 1, color: 'verde', model: 'urban' });

      Bike.add(aBike, (err, newBike) => {
        if (err) console.log(err);
      });

      request.get(base_url, (error, response, body) => {
        expect(response.statusCode).toBe(200);
        done();
      });
    });
  });

  // Test POST
  describe('POST /api/bicicletas/create', () => {
    it('Status 200', (done) => {
      Bike.find({}, (err, bikes) => {
        expect(bikes.length).toBe(0);
      });

      request.post(
        {
          headers: { 'content-type': 'application/json' },
          url: `${base_url}/create`,
          body: JSON.stringify({ code: 3, color: 'rojo', model: 'urbana', lat: 6.2648627, lon: -75.566595 })
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(200);
          const { bike } = JSON.parse(body);
          Bike.findOneAndDelete({ id: bike._id }, function (err, success) {
            if (err) console.log(err);
            done();
          });
        }
      );
    });
  });

  // Test Delete
  describe('DELETE /api/biciletas/delete', () => {
    it('Status 204', (done) => {
      Bike.find({}, (err, bikes) => {
        expect(bikes.length).toBe(0);
      });

      Bike.add(
        {
          code: 10,
          color: 'rojo',
          model: 'urbana',
          location: [6.2648627, -75.566595]
        },
        function (err, bike) {
          if (err) console.log(err);

          request.delete(
            {
              headers: { 'content-type': 'application/json' },
              url: 'http://localhost:3000/api/bicicletas/delete',
              body: JSON.stringify({ id: bike._id })
            },
            function (error, response, body) {
              expect(response.statusCode).toBe(204);
              done();
            }
          );
        }
      );
    });
  });
});
