var mongoose = require('mongoose');
const User = require('../../models/user');
const request = require('request');
let server = require('../../bin/www');

var base_url = 'http://localhost:3000/api/usuarios';

describe('User API Testing', () => {
  beforeAll((done) => {
    mongoose.connection.close(done);
  });

  beforeEach(function (done) {
    let mongoDB = 'mongodb://localhost/testdb';
    mongoose.connect(mongoDB, { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', function () {
      console.log('We are connected to test database!');
      done();
    });
  });

  afterEach(function (done) {
    User.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      mongoose.disconnect();
      done();
    });
  });

  // GET /api/usuarios
  describe('GET /api/usuarios', () => {
    it('Status 200', (done) => {
      User.allUsers((err, users) => {
        expect(users.length).toBe(0);
      });

      User.add({ name: 'Alejandro' }, (err, newUser) => {
        if (err) console.log(err);
      });

      request.get(base_url, function (error, response, body) {
        expect(response.statusCode).toBe(200);
        done();
      });
    });
  });

  // POST /api/usuarios/create
  describe('POST /api/usuarios/create', () => {
    it('Status 200', (done) => {
      User.allUsers((err, users) => {
        expect(users.length).toBe(0);
      });

      request.post(
        {
          headers: { 'content-type': 'application/json' },
          url: `${base_url}/create`,
          body: JSON.stringify({ name: 'Benjamin' })
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(200);

          const { user } = JSON.parse(body);
          User.findByIdAndDelete(user._id, function (err, success) {
            if (err) console.log(err);
            done();
          });
        }
      );
    });
  });

  describe('DELETE /api/usuarios/delete', () => {
    it('Status 204', (done) => {
      User.allUsers((err, users) => {
        expect(users.length).toBe(0);
      });

      User.add({ name: 'Joaquin' }, function (err, user) {
        if (err) console.log(err);

        request.delete(
          {
            headers: { 'content-type': 'application/json' },
            url: `${base_url}/delete`,
            body: JSON.stringify({ id: user._id })
          },
          function (error, response, body) {
            expect(response.statusCode).toBe(204);
            done();
          }
        );
      });
    });
  });
});
