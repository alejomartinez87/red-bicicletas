var mongoose = require('mongoose');
const Reservation = require('../../models/reservation');
const request = require('request');
let server = require('../../bin/www');
const reservation = require('../../models/reservation');

var base_url = 'http://localhost:3000/api/reservas';

describe('Reservation API', () => {
  beforeAll((done) => {
    mongoose.connection.close(done);
  });

  beforeEach(function (done) {
    let mongoDB = 'mongodb://localhost/testdb';
    mongoose.connect(mongoDB, { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', function () {
      console.log('We are connected to test database!');
      done();
    });
  });

  afterEach(function (done) {
    Reservation.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
      mongoose.disconnect();
    });
  });

  // GET /api/reservas
  describe('GET /api/reservas', () => {
    it('Status 200', (done) => {
      Reservation.find({}, (err, reservations) => {
        expect(reservations.length).toBe(0);
      });

      var aReservation = new Reservation({
        from: '2020-10-06',
        to: '2020-10-10',
        bike: '5f7c9b21d35fae382c943842',
        user: '5f7c9f3856c8f276bc81b462'
      });

      Reservation.add(aReservation, (err, newReservation) => {
        if (err) console.log(err);
      });

      request.get(base_url, function (error, response, body) {
        expect(response.statusCode).toBe(200);
        done();
      });
    });
  });

  // POST /api/reservas/create
  describe('POST /api/reservas/create', () => {
    it('Status 200', (done) => {
      Reservation.find({}, (err, reservations) => {
        expect(reservations.length).toBe(0);
      });

      request.post(
        {
          headers: { 'content-type': 'application/json' },
          url: `${base_url}/create`,
          body: JSON.stringify({ from: '2020-10-06', to: '2020-10-10', bike: '5f7c9b21d35fae382c943842', user: '5f7c9f3856c8f276bc81b462' })
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(200);
          const { reservation } = JSON.parse(body);
          Reservation.findByIdAndDelete(reservation._id, function (err, success) {
            if (err) console.log(err);
            done();
          });
        }
      );
    });
  });

  describe('DELETE /api/reservas/delete', () => {
    it('Status 204', (done) => {
      Reservation.find({}, (err, reservations) => {
        expect(reservations.length).toBe(0);
      });

      Reservation.add({ name: 'Joaquin' }, function (err, reservation) {
        if (err) console.log(err);

        request.delete(
          {
            headers: { 'content-type': 'application/json' },
            url: `${base_url}/delete`,
            body: `{ "id": "${reservation._id}" }`
          },
          function (error, response, body) {
            expect(response.statusCode).toBe(204);
            done();
          }
        );
      });
    });
  });
});
