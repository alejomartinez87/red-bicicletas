let Reservation = require('../../models/reservation');

exports.reservation_list = function (req, res) {
  Reservation.allReservations(function (err, reservations) {
    if (err) console.log(err);
    res.status(200).json({
      reservations: reservations
    });
  });
};

exports.reservation_create = function (req, res) {
  var reservation = {
    from: req.body.from,
    to: req.body.to,
    bike: req.body.bike,
    user: req.body.user
  };
  Reservation.add(reservation, function (err, newReservation) {
    if (err) console.log(err);
    res.status(200).json({
      reservation: newReservation
    });
  });
};

exports.reservation_delete = function (req, res) {
  Reservation.findByIdAndDelete(req.body.id, function (err, success) {
    if (err) {
      console.log(err);
    } else {
      res.status(204).send();
    }
  });
};
