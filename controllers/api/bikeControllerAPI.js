var Bike = require('../../models/bike');

exports.bike_list = function (req, res) {
  Bike.find({}, function (err, bikes) {
    if (err) console.log(err);
    res.status(200).json({
      bikes: bikes
    });
  });
};

exports.bike_create = function (req, res) {
  var bike = { code: req.body.id, color: req.body.color, model: req.body.model };
  bike.ubication = [req.body.lat, req.body.lon];
  Bike.add(bike, (err, newBike) => {
    if (err) console.log(err);
    res.status(200).json({
      bike: newBike
    });
  });
};

exports.bike_delete = function (req, res) {
  Bike.findByIdAndRemove(req.body.id, function (err, bike) {
    if (err) console.log(err);
    res.status(204).send();
  });
};
