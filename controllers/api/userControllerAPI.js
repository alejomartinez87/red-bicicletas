let User = require('../../models/user');

exports.user_list = function (req, res) {
  User.allUsers(function (err, users) {
    if (err) console.log(err);
    res.status(200).json({
      users: users
    });
  });
};

exports.user_create = function (req, res) {
  var user = { name: req.body.name, email: req.body.email, password: req.body.password };
  User.add(user, function (err, newUser) {
    if (err) console.log(err);
    res.status(200).json({
      user: newUser
    });
  });
};

exports.user_delete = function (req, res) {
  User.findByIdAndDelete(req.body.id, function (err, success) {
    if (err) {
      console.log(err);
    } else {
      res.status(204).send();
    }
  });
};
