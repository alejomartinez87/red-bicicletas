const User = require('../../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = {
  authenticate: function (req, res, next) {
    User.findOne({ email: req.body.email }, function (err, userInfo) {
      if (err) {
        next(err);
      } else {
        if (userInfo === null) return res.status(401).json({ status: 'error', message: 'Invalid email', data: null });
        if (userInfo !== null && bcrypt.compareSync(req.body.password, userInfo.password)) {
          userInfo.save(function (err, user) {
            const token = jwt.sign({ id: user._id }, req.app.get('secretKey'), { expiresIn: '7d' });
            return res.status(200).json({ message: 'usuario encontrado', data: { user: user, token: token } });
          });
        } else {
          res.status(401).json({ status: 'error', message: 'Invalid password', data: null });
        }
      }
    });
  },
  forgotPassword: function (req, res, next) {
    User.findOne({ email: req.body.email }, function (err, user) {
      if (err) {
        next(err);
      } else if (!user) {
        return res.status(401).json({ message: 'No existe el usuario', data: null });
      } else {
        user.resetPassword(function (err) {
          if (err) {
            next(err);
          } else {
            return res.status(200).json({ message: 'Se envio un email para restablecer contraseña', data: null });
          }
        });
      }
    });
  },
  authFacebookToken: function (req, res, next) {
    if (req.user) {
      req.user
        .save()
        .then(() => {
          const token = jwt.sign({ id: req.user._id }, req.app.get('secretKey'), { expiresIn: '7d' });
          return res.status(200).json({ message: 'usuario encontrado por Facebook Token', data: { user: req.user, token: token } });
        })
        .catch((err) => {
          console.log(err);
          res.status(500).json({ message: err.message });
        });
    } else {
      res.status(401);
    }
  }
};
