const Bike = require('../models/bike');

exports.bike_list = function (req, res) {
  Bike.find({}, function (err, bikes) {
    if (err) console.log(err);
    res.render('bikes/index', { bikes: bikes });
  });
};

exports.bike_create_get = function (req, res) {
  res.render('bikes/create');
};

exports.bike_create_post = function (req, res) {
  Bike.create(
    {
      color: req.body.color,
      model: req.body.model,
      ubication: [req.body.lat, req.body.lon]
    },
    function (err, bike) {
      if (err) console.log(err);
      // else console.log(bike);
      res.redirect('/bicicletas');
    }
  );
};

exports.bike_update_get = function (req, res) {
  Bike.findById(req.params.id, function (err, bike) {
    if (err) console.log(err);
    res.render('bikes/update', { bike });
  });
};

exports.bike_update_post = function (req, res) {
  var bike = Bike.findById(req.params.id);
  bike.id = req.body.id;
  bike.color = req.body.color;
  bike.model = req.body.model;
  bike.ubication = [req.body.lat, req.body.long];

  res.redirect('/bicicletas');
};

exports.bike_delete_post = function (req, res) {
  Bike.findByIdAndDelete(req.body.id, function (err) {
    if (err) {
      next(err);
    } else {
      res.redirect('/bicicletas');
    }
  });
};
