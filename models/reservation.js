var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema;

var reservationSchema = new Schema({
  from: Date,
  to: Date,
  bike: { type: mongoose.Schema.Types.ObjectId, ref: 'Bike' },
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'Reservation' }
});

reservationSchema.statics.allReservations = function (cb) {
  return this.find({}, cb);
};

reservationSchema.statics.add = function (reservation, cb) {
  this.create(reservation, cb);
};

reservationSchema.methods.dayOfReservation = function () {
  return moment(this.to).diff(moment(this.from), 'days') + 1;
};

module.exports = mongoose.model('Reservation', reservationSchema);
