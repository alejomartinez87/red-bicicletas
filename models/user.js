var mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
var Reservation = require('./reservation');
var Token = require('./token');
var Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const mailer = require('../mailer/mailer');
const crypto = require('crypto');

const saltRounds = 10;

const validateEmail = function (email) {
  const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
};

var userSchema = new Schema({
  name: {
    type: String,
    trim: true,
    required: [true, 'El nombre es obligatorio']
  },
  email: {
    type: String,
    trim: true,
    required: [true, 'El email es requerido'],
    lowercase: true,
    unique: true,
    validate: [validateEmail, 'Por favor ingresa un email válido'],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
  },
  password: {
    type: String,
    required: [true, 'El password es obligatorio']
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verified: {
    type: Boolean,
    default: false
  },
  googleId: String,
  facebookId: String
});

userSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario' });

userSchema.pre('save', function (next) {
  if (this.isModified('password')) {
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }
  next();
});

userSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};

userSchema.statics.allUsers = function (cb) {
  return this.find({}, cb);
};

userSchema.statics.add = function (user, cb) {
  this.create(user, cb);
};

userSchema.reservation = function (bikeId, from, to, cb) {
  var reservation = new Reservation({ user: this._id, bike: bikeId, from: from, to: to });
  console.log(reservation);
  reservation.save(cb);
};

userSchema.methods.sendWelcomeEmail = function (cb) {
  const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex') });
  console.log(token);

  const email_destination = this.email;
  token.save(function (err) {
    if (err) {
      return console.log(err.message);
    }

    const verifyUrl = `http://localhost:3000/token/confirmation/${token.token}`;

    const mailOptions = {
      from: 'no-reply@redbicicletas.com',
      to: email_destination,
      subject: 'Verificación de cuenta',
      html: `<p>Hola, \n\n Por favor, para verficiar su cuenta haz click en este enlace:</p> <a href="${verifyUrl}">${verifyUrl}</a>`
    };

    mailer.sendMail(mailOptions, function (err) {
      if (err) {
        return console.log(err.message);
      }

      console.log('Se ha enviado un email de bienvida a ' + email_destination);
    });
  });
};

userSchema.methods.resetPassword = function (cb) {
  const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex') });
  const email_destination = this.email;
  token.save(function (err) {
    if (err) return cb(err);

    const verifyUrl = `http://localhost:3000/resetpassword/${token.token}`;

    const mailOptions = {
      from: 'no-reply@redbicicletas.com',
      to: email_destination,
      subject: 'Restablecer Contraseña',
      html: `<p>Hola, \n\n Por favor, para restablecer el password de su cuenta haz click en este enlace:</p> <a href="${verifyUrl}">${verifyUrl}</a>`
    };

    mailer.sendMail(mailOptions, function (err) {
      if (err) return cb(err);
      console.log('Se envío correo para restablecer contraseña');
    });

    cb(null);
  });
};

userSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(profileGoogle, callback) {
  const self = this;
  console.log('findOneOrCreateByGoogle: ', profileGoogle);
  self.findOne(
    {
      $or: [{ googleId: profileGoogle.id }, { email: profileGoogle.emails[0].value }]
    },
    function (err, result) {
      console.log('findOne: err,result', err, result);
      if (result !== null) {
        callback(err, result); // if exist on mongoDB
      } else {
        let newUser = {};
        newUser.googleId = profileGoogle.id;
        newUser.name = profileGoogle.displayName || 'No name';
        newUser.email = profileGoogle.emails[0].value;
        newUser.verified = true;
        newUser.password = crypto.randomBytes(16).toString('hex');
        self.create(newUser, (err, res) => {
          if (err) {
            console.error(err);
          }
          // console.log('create new user', res);
          callback(err, res);
        });
      }
    }
  );
};

userSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(profile, callback) {
  const self = this;
  console.log('findOneOrCreateByFacebook: ', profile);
  self.findOne(
    {
      $or: [{ facebookId: profile.id }, { email: profile.emails[0].value }]
    },
    function (err, result) {
      console.log('findOne: err,result', err, result);
      if (result !== null) {
        callback(err, result); // if exist on mongoDB
      } else {
        let newUser = {};
        newUser.facebookId = profile.id;
        newUser.name = profile.displayName || 'No name';
        newUser.email = profile.emails[0].value;
        newUser.verified = true;
        newUser.password = crypto.randomBytes(16).toString('hex');
        self.create(newUser, (err, res) => {
          if (err) {
            console.error(err);
          }
          // console.log('create new user', res);
          callback(err, res);
        });
      }
    }
  );
};

module.exports = mongoose.model('User', userSchema);
