var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bikeSchema = new Schema({
  code: Number,
  color: String,
  model: String,
  ubication: {
    type: [Number],
    index: { type: '2dsphere', sparse: true }
  }
});

bikeSchema.statics.createInstance = function (code, color, model, ubication) {
  return new this({
    code: code,
    color: color,
    model,
    model,
    ubication: ubication
  });
};

bikeSchema.methods.toString = function () {
  return 'id: ' + this.id + '| color: ' + this.color;
};

bikeSchema.statics.allBikes = function (cb) {
  return this.find({}, cb);
};

bikeSchema.statics.add = function (bike, cb) {
  this.create(bike, cb);
};

bikeSchema.statics.findByCode = function (aCode, cb) {
  return this.findOne({ code: aCode }, cb);
};

bikeSchema.statics.removeByCode = function (aCode, cb) {
  return this.deleteOne({ code: aCode }, cb);
};

module.exports = mongoose.model('Bike', bikeSchema);
